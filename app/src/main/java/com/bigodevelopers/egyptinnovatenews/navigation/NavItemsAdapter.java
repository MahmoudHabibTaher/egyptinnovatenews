package com.bigodevelopers.egyptinnovatenews.navigation;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bigodevelopers.egyptinnovatenews.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mahmoud on 2/2/17.
 */

public class NavItemsAdapter extends RecyclerView.Adapter<NavItemsAdapter.NavItemViewHolder> {

    private List<NavItem> mNavItems;
    private Context mContext;
    private LayoutInflater mInflater;
    private OnItemSelectedListener mOnItemSelectedListener;

    public NavItemsAdapter(Context context, List<NavItem> items) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        mNavItems = items;
    }

    @Override
    public NavItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NavItemViewHolder(
                mInflater.inflate(R.layout.layout_nav_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(NavItemViewHolder holder, int position) {
        NavItem navItem = mNavItems.get(position);
        holder.mIconImageView.setImageResource(navItem.getImageResId());
        holder.mTitleTextView.setText(navItem.getTitle());
        holder.itemView.setOnClickListener(view -> {
            if (mOnItemSelectedListener != null) {
                mOnItemSelectedListener.onItemSelected(navItem);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mNavItems.size();
    }

    public void setOnItemSelectedListener(OnItemSelectedListener listener) {
        mOnItemSelectedListener = listener;
    }

    public interface OnItemSelectedListener {
        void onItemSelected(NavItem navItem);
    }

    public static class NavItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.nav_ic_image_view)
        ImageView mIconImageView;

        @BindView(R.id.nav_title_text_view)
        TextView mTitleTextView;

        public NavItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
