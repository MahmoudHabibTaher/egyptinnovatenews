package com.bigodevelopers.egyptinnovatenews.navigation;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bigodevelopers.egyptinnovatenews.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by mahmoud on 2/2/17.
 */

public class NavigationFragment extends Fragment implements NavItemsAdapter.OnItemSelectedListener {
    @BindView(R.id.nav_items_recycler_view)
    RecyclerView mNavItemsRecyclerView;

    private List<NavItem> mNavItems;
    private NavItemsAdapter mAdapter;

    private Unbinder mUnbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_navigation, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getView() != null) {
            mUnbinder = ButterKnife.bind(this, getView());
            init();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    private void init() {
        mNavItemsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false));

        mNavItems = new ArrayList<>();

        loadNavItems();

        mAdapter = new NavItemsAdapter(getContext(), mNavItems);
        mAdapter.setOnItemSelectedListener(this);
        mNavItemsRecyclerView.setAdapter(mAdapter);
    }

    private void loadNavItems() {
        TypedArray typedArray = getResources().obtainTypedArray(R.array.nav_items_icons);
        String[] navTitles = getResources().getStringArray(R.array.nav_items_titles);

        for (int i = 0; i < navTitles.length; i++) {
            NavItem navItem = new NavItem();
            int iconRes = typedArray.getResourceId(i, -1);
            String title = navTitles[i];
            navItem.setId(i);
            navItem.setImageResId(iconRes);
            navItem.setTitle(title);
            mNavItems.add(navItem);
        }

        typedArray.recycle();
    }

    @Override
    public void onItemSelected(NavItem navItem) {

    }
}
