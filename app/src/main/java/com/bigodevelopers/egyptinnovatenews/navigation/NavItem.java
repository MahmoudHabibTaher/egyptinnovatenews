package com.bigodevelopers.egyptinnovatenews.navigation;

/**
 * Created by mahmoud on 2/2/17.
 */

public class NavItem {
    private int id;
    private String title;
    private int imageResId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImageResId() {
        return imageResId;
    }

    public void setImageResId(int imageResId) {
        this.imageResId = imageResId;
    }
}
