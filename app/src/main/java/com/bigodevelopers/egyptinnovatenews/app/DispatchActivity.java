package com.bigodevelopers.egyptinnovatenews.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.bigodevelopers.egyptinnovatenews.news.NewsActivity;

public class DispatchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivity(NewsActivity.getStartIntent(this));
        finish();
    }
}
