package com.bigodevelopers.egyptinnovatenews.news.data.source.remote;

import com.bigodevelopers.egyptinnovatenews.news.data.News;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mahmoud on 2/2/17.
 */

public class NewsDetailsResponse {
    @SerializedName("newsItem")
    private News news;

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }
}
