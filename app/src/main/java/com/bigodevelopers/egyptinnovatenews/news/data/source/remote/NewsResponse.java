package com.bigodevelopers.egyptinnovatenews.news.data.source.remote;

import com.bigodevelopers.egyptinnovatenews.news.data.News;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mahmoud on 2/2/17.
 */

public class NewsResponse {
    @SerializedName("News")
    private List<News> newsList;

    public List<News> getNewsList() {
        return newsList;
    }

    public void setNewsList(List<News> newsList) {
        this.newsList = newsList;
    }
}
