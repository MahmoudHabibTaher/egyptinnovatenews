package com.bigodevelopers.egyptinnovatenews.news;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;

import com.bigodevelopers.egyptinnovatenews.R;
import com.bigodevelopers.egyptinnovatenews.common.ui.BaseActivity;
import com.bigodevelopers.egyptinnovatenews.news.data.source.NewsRepository;
import com.bigodevelopers.egyptinnovatenews.news.data.source.remote.NewsRemoteDataSource;
import com.bigodevelopers.egyptinnovatenews.news.list.NewsListFragment;
import com.bigodevelopers.egyptinnovatenews.news.list.NewsListPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mahmoud on 2/1/17.
 */

public class NewsActivity extends BaseActivity {
    public static Intent getStartIntent(Context context) {
        return new Intent(context, NewsActivity.class);
    }

    private static final String TAG = NewsActivity.class.getSimpleName();

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    private ActionBarDrawerToggle mDrawerToggle;

    private NewsListPresenter mNewsListPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        ButterKnife.bind(this);
        init();
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        mDrawerToggle.syncState();
    }

    private void init() {
        setToolbarTitle(R.string.news_activity_title);

        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                getToolbar(),
                R.string.drawer_open,
                R.string.drawer_close);

        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        NewsListFragment newsListFragment = (NewsListFragment) getSupportFragmentManager()
                .findFragmentById(R.id.news_list_fragment_container);
        if (newsListFragment == null) {
            newsListFragment = NewsListFragment.newInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.news_list_fragment_container,
                    newsListFragment).commit();
        }

        mNewsListPresenter = new NewsListPresenter(NewsRepository.getInstance
                (NewsRemoteDataSource.getInstance()), newsListFragment);
    }
}
