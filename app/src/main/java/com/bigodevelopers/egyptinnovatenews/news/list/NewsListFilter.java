package com.bigodevelopers.egyptinnovatenews.news.list;

/**
 * Created by mahmoud on 2/14/17.
 */

public enum NewsListFilter {
    ALL(0),
    ARTICLES(84),
    VIDEOS(85);

    private int type;

    NewsListFilter(int type) {
        this.type = type;
    }

    int getType() {
        return type;
    }
}