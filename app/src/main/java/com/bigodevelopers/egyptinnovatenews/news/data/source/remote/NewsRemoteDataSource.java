package com.bigodevelopers.egyptinnovatenews.news.data.source.remote;

import android.support.annotation.NonNull;

import com.bigodevelopers.egyptinnovatenews.news.data.News;
import com.bigodevelopers.egyptinnovatenews.news.data.source.NewsDataSource;

import java.util.List;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

/**
 * Created by mahmoud on 2/2/17.
 */

public class NewsRemoteDataSource implements NewsDataSource {
    private static final String BASE_URL = "http://egyptinnovate.com/";

    private static NewsRemoteDataSource INSTANCE = null;

    public synchronized static NewsRemoteDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new NewsRemoteDataSource();
        }
        return INSTANCE;
    }

    private NewsApi mNewsApi;

    private NewsRemoteDataSource() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mNewsApi = retrofit.create(NewsApi.class);
    }

    @NonNull
    @Override
    public Observable<List<News>> getNewsList() {
        return mNewsApi.getNewsList()
                .flatMap(newsResponse -> Observable.just(newsResponse.getNewsList()));
    }

    @NonNull
    @Override
    public Observable<News> getNewsDetails(@NonNull String id) {
        return mNewsApi.getNewsDetails(id).flatMap(newsDetailsResponse -> Observable.just
                (newsDetailsResponse.getNews()));
    }
}
