package com.bigodevelopers.egyptinnovatenews.news.data.source;

import android.support.annotation.NonNull;

import com.bigodevelopers.egyptinnovatenews.news.data.News;

import java.util.List;

import rx.Observable;

/**
 * Created by mahmoud on 2/2/17.
 */

public class NewsRepository implements NewsDataSource {
    private static NewsRepository INSTANCE = null;

    public static NewsRepository getInstance(NewsDataSource remoteDataSource) {
        if (INSTANCE == null) {
            INSTANCE = new NewsRepository(remoteDataSource);
        }
        return INSTANCE;
    }

    private NewsDataSource mRemoteDataSource;

    private NewsRepository(NewsDataSource remoteDataSource) {
        mRemoteDataSource = remoteDataSource;
    }

    @NonNull
    @Override
    public Observable<List<News>> getNewsList() {
        return mRemoteDataSource.getNewsList();
    }

    @NonNull
    @Override
    public Observable<News> getNewsDetails(@NonNull String id) {
        return mRemoteDataSource.getNewsDetails(id);
    }
}
