package com.bigodevelopers.egyptinnovatenews.news.list;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;

import com.bigodevelopers.egyptinnovatenews.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.bigodevelopers.egyptinnovatenews.news.list.NewsListFilter.ALL;
import static com.bigodevelopers.egyptinnovatenews.news.list.NewsListFilter.VIDEOS;

/**
 * Created by mahmoud on 2/13/17.
 */

public class NewsFilterFragment extends Fragment {
    private static final long DEFAULT_ANIM_DURATION = 250;

    private static final String STATE_FILTER = "STATE_FILTER";
    private static final String STATE_SHOWN = "STATE_SHOWN";

    public static NewsFilterFragment newInstance() {
        return new NewsFilterFragment();
    }

    @BindView(R.id.main_content_layout)
    View mMainContentLayout;

    @BindView(R.id.article_filter_image_view)
    ImageView mArticleFilterImageView;

    @BindView(R.id.videos_filter_image_view)
    ImageView mVideosFilterImageView;

    private NewsListFilter mCurrentFilter;

    private boolean mShown;

    private OnFilterSelectedListener mOnFilterSelectedListener;

    private Unbinder mUnbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_news_filter, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            mCurrentFilter = (NewsListFilter) savedInstanceState.getSerializable(STATE_FILTER);
            mShown = savedInstanceState.getBoolean(STATE_SHOWN);
        } else {
            mCurrentFilter = ALL;
            mShown = false;
        }

        if (getView() != null) {
            mUnbinder = ButterKnife.bind(this, getView());
            init();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(STATE_FILTER, mCurrentFilter);
        outState.putBoolean(STATE_SHOWN, mShown);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }

    private void init() {
        mArticleFilterImageView.setOnClickListener(view -> onFilterClick(NewsListFilter.ARTICLES));

        mVideosFilterImageView.setOnClickListener(view -> onFilterClick(VIDEOS));

        switch (mCurrentFilter) {
            case ARTICLES:
                mArticleFilterImageView.setSelected(true);
                break;
            case VIDEOS:
                mVideosFilterImageView.setSelected(true);
                break;
        }

        if (isShown()) {
            show();
        }
    }

    private void onFilterClick(NewsListFilter filter) {
        switch (filter) {
            case ARTICLES: {
                mVideosFilterImageView.setSelected(false);
                boolean selected = mArticleFilterImageView.isSelected();
                selected = !selected;

                mArticleFilterImageView.setSelected(selected);
                if (selected) {
                    onFilterSelected(NewsListFilter.ARTICLES);
                } else {
                    onFilterSelected(NewsListFilter.ALL);
                }
            }
            break;
            case VIDEOS: {
                mArticleFilterImageView.setSelected(false);
                boolean selected = mVideosFilterImageView.isSelected();
                selected = !selected;

                mVideosFilterImageView.setSelected(selected);
                if (selected) {
                    onFilterSelected(NewsListFilter.VIDEOS);
                } else {
                    onFilterSelected(NewsListFilter.ALL);
                }
            }
            break;
        }
    }

    private void onFilterSelected(NewsListFilter filter) {
        mCurrentFilter = filter;
        if (mOnFilterSelectedListener != null) {
            mOnFilterSelectedListener.onFilterSelected(filter);
        }
    }

    public void show() {
        mShown = true;
        mMainContentLayout.animate().alpha(1.0f).setInterpolator(new
                AccelerateDecelerateInterpolator()).setDuration(DEFAULT_ANIM_DURATION)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        super.onAnimationStart(animation);
                        mMainContentLayout.setVisibility(View.VISIBLE);
                    }
                });
    }

    public void hide() {
        mShown = false;
        mMainContentLayout.animate().alpha(0.0f).setInterpolator(new
                AccelerateDecelerateInterpolator()).setDuration(DEFAULT_ANIM_DURATION).setListener(
                new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationCancel(Animator animation) {
                        super.onAnimationCancel(animation);
                        mMainContentLayout.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        mMainContentLayout.setVisibility(View.GONE);
                    }
                });
    }

    public boolean isShown() {
        return mShown;
    }

    public void setOnFilterSelectedListener(OnFilterSelectedListener listener) {
        mOnFilterSelectedListener = listener;
    }

    public interface OnFilterSelectedListener {
        void onFilterSelected(@NonNull NewsListFilter filter);
    }
}
