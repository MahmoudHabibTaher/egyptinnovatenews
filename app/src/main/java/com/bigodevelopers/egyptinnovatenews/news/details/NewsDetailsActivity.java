package com.bigodevelopers.egyptinnovatenews.news.details;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

import com.bigodevelopers.egyptinnovatenews.R;
import com.bigodevelopers.egyptinnovatenews.common.ui.BaseActivity;
import com.bigodevelopers.egyptinnovatenews.news.data.source.NewsRepository;
import com.bigodevelopers.egyptinnovatenews.news.data.source.remote.NewsRemoteDataSource;

/**
 * Created by mahmoud on 2/2/17.
 */

public class NewsDetailsActivity extends BaseActivity {
    private static final String TAG = NewsDetailsActivity.class.getSimpleName();

    private static final String EXTRA_NEWS_ID = "EXTRA_NEWS_ID";

    @NonNull
    public static Intent getStartIntent(@NonNull Context context, @NonNull String newsId) {
        Intent intent = new Intent(context, NewsDetailsActivity.class);
        intent.putExtra(EXTRA_NEWS_ID, newsId);
        return intent;
    }

    private String mNewsId;
    private NewsDetailsPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivity_news_details);
        init();
    }

    private void init() {
        mNewsId = getIntent().getStringExtra(EXTRA_NEWS_ID);

        setToolbarTitle(R.string.news_details_activity_title);
        setBackEnabled(true);

        NewsDetailsFragment newsDetailsFragment = (NewsDetailsFragment) getSupportFragmentManager
                ().findFragmentById(R.id.news_details_fragment_container);
        if (newsDetailsFragment == null) {
            newsDetailsFragment = NewsDetailsFragment.newInstance(mNewsId);
            getSupportFragmentManager().beginTransaction().add(R.id
                    .news_details_fragment_container, newsDetailsFragment).commit();
        }

        mPresenter = new NewsDetailsPresenter(mNewsId, NewsRepository.getInstance
                (NewsRemoteDataSource.getInstance()), newsDetailsFragment);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
