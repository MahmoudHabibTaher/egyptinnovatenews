package com.bigodevelopers.egyptinnovatenews.news.data.source.remote;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by mahmoud on 2/2/17.
 */

public interface NewsApi {
    @GET("en/api/v01/safe/GetNews")
    Observable<NewsResponse> getNewsList();

    @GET("en/api/v01/safe/GetNewsDetails")
    Observable<NewsDetailsResponse> getNewsDetails(@Query("nid") String id);
}
