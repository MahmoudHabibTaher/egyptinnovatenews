package com.bigodevelopers.egyptinnovatenews.news.list;

import android.support.annotation.NonNull;

import com.bigodevelopers.egyptinnovatenews.common.BasePresenter;
import com.bigodevelopers.egyptinnovatenews.common.BaseView;
import com.bigodevelopers.egyptinnovatenews.news.data.News;

import java.util.List;

/**
 * Created by mahmoud on 2/2/17.
 */

public interface NewsListContract {
    interface View extends BaseView<Presenter> {
        void showLoadingLayout(boolean show);

        void showNewsList(@NonNull List<News> newsList);

        void showLoadNewsError();

        void showNoNews();

        void showNewsDetailsUi(@NonNull String id);
    }

    interface Presenter extends BasePresenter {
        void loadNews();

        void openNewsDetails(@NonNull News news);

        void setFilter(@NonNull NewsListFilter filter);

        @NonNull
        NewsListFilter getFilter();
    }
}
