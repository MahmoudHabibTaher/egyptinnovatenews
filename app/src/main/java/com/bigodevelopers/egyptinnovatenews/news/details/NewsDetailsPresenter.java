package com.bigodevelopers.egyptinnovatenews.news.details;

import android.support.annotation.NonNull;
import android.util.Log;

import com.bigodevelopers.egyptinnovatenews.news.data.News;
import com.bigodevelopers.egyptinnovatenews.news.data.source.NewsDataSource;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by mahmoud on 2/2/17.
 */

public class NewsDetailsPresenter implements NewsDetailsContract.Presenter {
    private static final String TAG = NewsDetailsPresenter.class.getSimpleName();

    private String mNewsId;
    private NewsDataSource mNewsDataSource;
    private NewsDetailsContract.View mView;
    private CompositeSubscription mSubscription;

    public NewsDetailsPresenter(@NonNull String newsId, @NonNull NewsDataSource newsDataSource,
                                @NonNull NewsDetailsContract.View view) {
        mNewsId = newsId;
        mNewsDataSource = newsDataSource;
        mView = view;
        mView.setPresenter(this);
    }

    @Override
    public void subscribe() {
        mSubscription = new CompositeSubscription();
        loadNewsDetails(mNewsId);
    }

    @Override
    public void unSubscribe() {
        mSubscription.unsubscribe();
    }

    @Override
    public void loadNewsDetails(@NonNull String newsId) {
        mView.showLoadingLayout(true);

        if (mSubscription == null) {
            mSubscription = new CompositeSubscription();
        }

        mSubscription.clear();

        Subscription subscription = mNewsDataSource.getNewsDetails(mNewsId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(news -> {
                    mView.showNewsDetails(news);
                }, throwable -> {
                    Log.e(TAG, "Error Loading news details id " + mNewsId, throwable);
                    mView.showLoadNewsDetailsError();
                });

        mSubscription.add(subscription);
    }

    @Override
    public void shareNews(@NonNull News news) {
        if (news != null) {
            mView.showShareUi(news.getShareUrl());
        }
    }
}
