package com.bigodevelopers.egyptinnovatenews.news.list;

import android.support.annotation.NonNull;
import android.util.Log;

import com.bigodevelopers.egyptinnovatenews.news.data.News;
import com.bigodevelopers.egyptinnovatenews.news.data.source.NewsDataSource;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by mahmoud on 2/2/17.
 */

public class NewsListPresenter implements NewsListContract.Presenter {
    private static final String TAG = NewsListPresenter.class.getSimpleName();

    private NewsDataSource mNewsDataSource;
    private NewsListContract.View mView;
    private CompositeSubscription mSubscription;
    private NewsListFilter mFilter;
    private boolean mFirstLoad;

    public NewsListPresenter(NewsDataSource newsDataSource, NewsListContract.View view) {
        mNewsDataSource = newsDataSource;
        mView = view;
        mView.setPresenter(this);
        mFilter = NewsListFilter.ALL;
    }

    @Override
    public void subscribe() {
        mSubscription = new CompositeSubscription();
        loadNews();
    }

    @Override
    public void unSubscribe() {
        mSubscription.unsubscribe();
    }

    @Override
    public void loadNews() {
        if (mFirstLoad) {
            mView.showLoadingLayout(true);
        }
        mFirstLoad = false;

        if (mSubscription == null) {
            mSubscription = new CompositeSubscription();
        }

        mSubscription.clear();

        Subscription subscription = mNewsDataSource.getNewsList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(newsList -> Observable.from(newsList).filter(news -> {
                    if (mFilter == NewsListFilter.ALL) {
                        return true;
                    }
                    int type = news.getNewsType();
                    return type == mFilter.getType();
                }).toList())
                .subscribe(newsList -> {
                    if (newsList != null && !newsList.isEmpty()) {
                        mView.showNewsList(newsList);
                    } else {
                        mView.showNoNews();
                    }
                }, throwable -> {
                    Log.e(TAG, "Error loading news", throwable);
                    mView.showLoadNewsError();
                });
        mSubscription.add(subscription);
    }

    @Override
    public void openNewsDetails(@NonNull News news) {
        mView.showNewsDetailsUi(news.getId());
    }

    @Override
    public void setFilter(@NonNull NewsListFilter filter) {
        mFilter = filter;
    }

    @NonNull
    @Override
    public NewsListFilter getFilter() {
        return mFilter;
    }
}
