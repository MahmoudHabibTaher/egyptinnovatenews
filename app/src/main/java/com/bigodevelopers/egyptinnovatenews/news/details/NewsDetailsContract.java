package com.bigodevelopers.egyptinnovatenews.news.details;

import android.support.annotation.NonNull;

import com.bigodevelopers.egyptinnovatenews.common.BasePresenter;
import com.bigodevelopers.egyptinnovatenews.common.BaseView;
import com.bigodevelopers.egyptinnovatenews.news.data.News;

/**
 * Created by mahmoud on 2/2/17.
 */

public interface NewsDetailsContract {
    interface View extends BaseView<Presenter> {
        void showLoadingLayout(boolean show);

        void showNewsDetails(@NonNull News news);

        void showLoadNewsDetailsError();

        void showShareUi(@NonNull String url);
    }

    interface Presenter extends BasePresenter {
        void loadNewsDetails(@NonNull String newsId);

        void shareNews(@NonNull News news);
    }
}
