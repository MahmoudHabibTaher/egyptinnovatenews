package com.bigodevelopers.egyptinnovatenews.news.details;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bigodevelopers.egyptinnovatenews.R;
import com.bigodevelopers.egyptinnovatenews.news.data.News;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by mahmoud on 2/2/17.
 */

public class NewsDetailsFragment extends Fragment implements NewsDetailsContract.View {
    private static final String TAG = NewsDetailsFragment.class.getSimpleName();

    private static final String ARG_NEWS_ID = "ARG_NEWS_ID";

    public static NewsDetailsFragment newInstance(@NonNull String newsId) {
        NewsDetailsFragment fragment = new NewsDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_NEWS_ID, newsId);
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    @BindView(R.id.main_content_layout)
    View mMainContentLayout;

    @BindView(R.id.news_pic_image_view)
    ImageView mNewsPicImageView;

    @BindView(R.id.news_title_text_view)
    TextView mNewsTitleTextView;

    @BindView(R.id.news_date_text_view)
    TextView mNewsDateTextView;

    @BindView(R.id.num_likes_text_view)
    TextView mNumLikesTextView;

    @BindView(R.id.num_views_text_view)
    TextView mNumViewsTextView;

    @BindView(R.id.news_description_text_view)
    TextView mNewsDescriptionTextView;

    private String mNewsId;
    private News mNews;

    private NewsDetailsContract.Presenter mPresenter;

    private Unbinder mUnbinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            mNewsId = args.getString(ARG_NEWS_ID);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_news_details, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getView() != null) {
            setHasOptionsMenu(true);
            mUnbinder = ButterKnife.bind(this, getView());
            init();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.news_details_fragment_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share:
                mPresenter.shareNews(mNews);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unSubscribe();
    }

    private void init() {

    }

    @Override
    public void showLoadingLayout(boolean show) {
        int visibility = show ? View.VISIBLE : View.GONE;
        mProgressBar.setVisibility(visibility);

        if (show) {
            mMainContentLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void showNewsDetails(@NonNull News news) {
        mNews = news;

        mNewsTitleTextView.setText(news.getTitle());

        Glide.with(this).load(news.getImageUrl()).placeholder(R.drawable.details_placeholder)
                .crossFade().centerCrop().into(mNewsPicImageView);

        mNewsDateTextView.setText(news.getDate());
        mNumLikesTextView.setText(getString(R.string.news_num_likes_string_holder, news
                .getNumLikes()));
        mNumViewsTextView.setText(getString(R.string.news_num_views_string_holder, news
                .getNumViews()));

        mNewsDescriptionTextView.setText(news.getDescription());

        mProgressBar.setVisibility(View.GONE);
        mMainContentLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoadNewsDetailsError() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showShareUi(@NonNull String url) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, url);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
    }

    @Override
    public void setPresenter(NewsDetailsContract.Presenter presenter) {
        mPresenter = presenter;
    }
}
