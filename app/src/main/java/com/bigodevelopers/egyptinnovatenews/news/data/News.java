package com.bigodevelopers.egyptinnovatenews.news.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mahmoud on 2/2/17.
 */

public class News {
    public static class Type {
        public static final int ARTICLE = 84;
        public static final int VIDEO = 85;
    }

    @SerializedName("Nid")
    private String id;

    @SerializedName("NewsTitle")
    private String title;

    @SerializedName("ItemDescription")
    private String description;

    @SerializedName("PostDate")
    private String date;

    @SerializedName("ImageUrl")
    private String imageUrl;

    @SerializedName("NewsType")
    private int newsType;

    @SerializedName("NumofViews")
    private String numViews;

    @SerializedName("Likes")
    private String numLikes;

    @SerializedName("VideoURL")
    private String videoUrl;

    @SerializedName("ShareURL")
    private String shareUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getNewsType() {
        return newsType;
    }

    public void setNewsType(int newsType) {
        this.newsType = newsType;
    }

    public String getNumViews() {
        return numViews;
    }

    public void setNumViews(String numViews) {
        this.numViews = numViews;
    }

    public String getNumLikes() {
        return numLikes;
    }

    public void setNumLikes(String numLikes) {
        this.numLikes = numLikes;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }
}
