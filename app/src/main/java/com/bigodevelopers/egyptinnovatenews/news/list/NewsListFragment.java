package com.bigodevelopers.egyptinnovatenews.news.list;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.bigodevelopers.egyptinnovatenews.R;
import com.bigodevelopers.egyptinnovatenews.news.data.News;
import com.bigodevelopers.egyptinnovatenews.news.details.NewsDetailsActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by mahmoud on 2/1/17.
 */

public class NewsListFragment extends Fragment implements NewsListContract.View, NewsAdapter
        .OnNewsSelectedListener, NewsFilterFragment.OnFilterSelectedListener {
    private static final String TAG = NewsListFragment.class.getSimpleName();

    private static final String STATE_FILTER = "STATE_FILTER";

    public static NewsListFragment newInstance() {
        return new NewsListFragment();
    }

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    @BindView(R.id.news_recycler_view)
    RecyclerView mNewsRecyclerView;

    private List<News> mNews;
    private NewsAdapter mAdapter;

    private NewsListFilter mCurrentFilter;
    private NewsFilterFragment mNewsFilterFragment;

    private Unbinder mUnbinder;

    private NewsListContract.Presenter mPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_news_list, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            mCurrentFilter = (NewsListFilter) savedInstanceState.getSerializable(STATE_FILTER);
            mPresenter.setFilter(mCurrentFilter);
        } else {
            mCurrentFilter = mPresenter.getFilter();
        }
        if (getView() != null) {
            setHasOptionsMenu(true);
            mUnbinder = ButterKnife.bind(this, getView());
            init();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.news_list_fragment_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_filter:
                onFilterClick();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(STATE_FILTER, mCurrentFilter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unSubscribe();
    }

    private void init() {
        mNewsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false));
        mNews = new ArrayList<>();
        mAdapter = new NewsAdapter(getContext(), mNews);
        mAdapter.setOnNewsSelectedListener(this);
        mNewsRecyclerView.setAdapter(mAdapter);

        mNewsFilterFragment = (NewsFilterFragment) getChildFragmentManager().findFragmentById(R.id
                .news_filter_fragment_container);
        if (mNewsFilterFragment == null) {
            mNewsFilterFragment = NewsFilterFragment.newInstance();
            getChildFragmentManager().beginTransaction().add(R.id.news_filter_fragment_container,
                    mNewsFilterFragment).commit();
        }
        mNewsFilterFragment.setOnFilterSelectedListener(this);
    }

    @Override
    public void onNewsSelected(@NonNull News news) {
        mPresenter.openNewsDetails(news);
    }

    @Override
    public void showLoadingLayout(boolean show) {
        int visibility = show ? View.VISIBLE : View.GONE;
        mProgressBar.setVisibility(visibility);
        if (show) {
            mNewsRecyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void showNewsList(@NonNull List<News> newsList) {
        mNews.clear();
        mNews.addAll(newsList);
        mAdapter.notifyDataSetChanged();
        mNewsRecyclerView.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showLoadNewsError() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showNoNews() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showNewsDetailsUi(@NonNull String id) {
        startActivity(NewsDetailsActivity.getStartIntent(getContext(), id));
    }

    @Override
    public void setPresenter(NewsListContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onFilterSelected(@NonNull NewsListFilter filter) {
        hideFilter();
        mCurrentFilter = filter;
        mPresenter.setFilter(filter);
        mPresenter.loadNews();
    }

    public void showFilter() {
        if (mNewsFilterFragment != null) {
            mNewsFilterFragment.show();
        }
    }

    private void hideFilter() {
        if (mNewsFilterFragment != null) {
            mNewsFilterFragment.hide();
        }
    }

    private void onFilterClick() {
        if (mNewsFilterFragment != null && mNewsFilterFragment.isShown()) {
            hideFilter();
        } else {
            showFilter();
        }
    }
}
