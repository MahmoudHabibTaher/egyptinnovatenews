package com.bigodevelopers.egyptinnovatenews.news.list;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bigodevelopers.egyptinnovatenews.R;
import com.bigodevelopers.egyptinnovatenews.news.data.News;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mahmoud on 2/1/17.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {
    private List<News> mNewsList;
    private Context mContext;
    private LayoutInflater mInflater;
    private OnNewsSelectedListener mOnNewsSelectedListener;

    public NewsAdapter(Context context, List<News> newsList) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        mNewsList = newsList;
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NewsViewHolder(mInflater.inflate(R.layout.layout_news_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {
        News news = mNewsList.get(position);

        Glide.with(mContext).load(news.getImageUrl()).dontAnimate().into(holder.mPicImageView);

        holder.mTitleTextView.setText(news.getTitle());

        holder.mDateTextView.setText(news.getDate());

        holder.mNumLikesTextView.setText(mContext.getString(R.string
                .news_num_likes_string_holder, news.getNumLikes()));

        holder.mNumViewsTextView.setText(mContext.getString(R.string
                .news_num_views_string_holder, news.getNumViews()));

        int typeImageRes = getTypeImage(news.getNewsType());
        holder.mTypeImageView.setImageResource(typeImageRes);

        holder.itemView.setOnClickListener(view -> {
            if (mOnNewsSelectedListener != null) {
                mOnNewsSelectedListener.onNewsSelected(news);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mNewsList.size();
    }

    private int getTypeImage(int type) {
        int imageRes = R.drawable.article_label;
        switch (type) {
            case News.Type.ARTICLE:
                imageRes = R.drawable.article_label;
                break;
            case News.Type.VIDEO:
                imageRes = R.drawable.video_label;
                break;
        }
        return imageRes;
    }

    public void setOnNewsSelectedListener(OnNewsSelectedListener listener) {
        mOnNewsSelectedListener = listener;
    }

    public interface OnNewsSelectedListener {
        void onNewsSelected(@NonNull News news);
    }

    public static class NewsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.news_pic_image_view)
        ImageView mPicImageView;
        @BindView(R.id.news_title_text_view)
        TextView mTitleTextView;
        @BindView(R.id.news_date_text_view)
        TextView mDateTextView;
        @BindView(R.id.num_likes_text_view)
        TextView mNumLikesTextView;
        @BindView(R.id.num_views_text_view)
        TextView mNumViewsTextView;
        @BindView(R.id.news_type_text_view)
        ImageView mTypeImageView;

        public NewsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
