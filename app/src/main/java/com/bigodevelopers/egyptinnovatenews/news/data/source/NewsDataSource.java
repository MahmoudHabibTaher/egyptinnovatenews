package com.bigodevelopers.egyptinnovatenews.news.data.source;

import android.support.annotation.NonNull;

import com.bigodevelopers.egyptinnovatenews.news.data.News;

import java.util.List;

import rx.Observable;

/**
 * Created by mahmoud on 2/2/17.
 */

public interface NewsDataSource {
    @NonNull
    Observable<List<News>> getNewsList();

    @NonNull
    Observable<News> getNewsDetails(@NonNull String id);
}
