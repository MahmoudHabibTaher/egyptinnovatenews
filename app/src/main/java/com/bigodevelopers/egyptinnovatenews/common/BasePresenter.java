package com.bigodevelopers.egyptinnovatenews.common;

/**
 * Created by mahmoud on 2/2/17.
 */

public interface BasePresenter {
    void subscribe();

    void unSubscribe();
}
