package com.bigodevelopers.egyptinnovatenews.common;

/**
 * Created by mahmoud on 2/2/17.
 */

public interface BaseView<T extends BasePresenter> {
    void setPresenter(T presenter);
}
